<?php


class Teacher extends Person
{
    protected $subject ;
    public function __construct($fio, $phone, $mail, $role, $subject)
    {
        parent::__construct($fio, $phone, $mail, $role);
        $this->subject = htmlspecialchars($subject);
    }
    public function addTeacher(PDO $pdo){
        try {
            $sql = 'INSERT INTO members SET
    full_name = :full_name,
    phone = :phone,
    email = :email,
    role = :role,
    subject = :subject,
    date_created = CURDATE();
';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':full_name',$this->fio);
            $statement->bindValue(':phone',$this->phone);
            $statement->bindValue(':email',$this->mail);
            $statement->bindValue(':role',$this->role);
            $statement->bindValue(':subject',$this->subject);
            $statement->execute();
        }catch (Exception $exception){
            die('Error Add Teacher'. $exception->getCode() . $exception->getMessage());
        }
    }

    public function getSubject()
    {
        return $this->subject;
    }
    public function getVisitCard(){
        $str = $this->getFio() . '<br>';
        $str.= $this->getRole() . '<br>';
        $str.= $this->getMail() . '<br>';
        $str.= $this->getPhone() . '<br>';
        $str.= $this->getSubject() . '<br>';
    }


}