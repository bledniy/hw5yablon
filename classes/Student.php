<?php


class Student extends Person
{
    protected $mark ;

    public function __construct($fio, $phone, $mail, $role, $mark)
    {
        parent::__construct($fio, $phone, $mail, $role);
        $this->mark = $mark;
    }
    public function addStudent(PDO $pdo){
        try {
            $sql = 'INSERT INTO members SET
    full_name = :full_name,
    phone = :phone,
    email = :email,
    role = :role,
    averange_mark = :averange_mark,
    date_created = CURDATE();
';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':full_name',$this->fio);
            $statement->bindValue(':phone',$this->phone);
            $statement->bindValue(':email',$this->mail);
            $statement->bindValue(':role',$this->role);
            $statement->bindValue(':averange_mark',$this->mark);
            $statement->execute();
        }catch (Exception $exception){
            die('Error Add Student'. $exception->getCode() . $exception->getMessage());
        }
    }

    public function getMark()
    {
        return $this->mark;
    }
    public function getVisitCard(){
        $str = $this->getFio() . '<br>';
        $str.= $this->getRole() . '<br>';
        $str.= $this->getMail() . '<br>';
        $str.= $this->getPhone() . '<br>';
        $str.= $this->getMark() . '<br>';
        return $str;
    }

}
