<?php


class Admin extends Person
{
    protected $workingDay ;

    public function __construct($fio, $phone, $mail, $role, $workingDay)
    {
        parent::__construct($fio, $phone, $mail, $role);
        $this->workingDay = htmlspecialchars($workingDay);
    }
    public function addAdmin(PDO $pdo){
        try {
            $sql = 'INSERT INTO members SET
    full_name = :full_name,
    phone = :phone,
    email = :email,
    role = :role,
    working_day = :working_day,
    date_created = CURDATE();
';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':full_name',$this->fio);
            $statement->bindValue(':phone',$this->phone);
            $statement->bindValue(':email',$this->mail);
            $statement->bindValue(':role',$this->role);
            $statement->bindValue(':working_day',$this->workingDay);
            $statement->execute();
        }catch (Exception $exception){
            die('Error Add Admin'. $exception->getCode() . $exception->getMessage());
        }
    }

    public function getWorkingDay()
    {
        return $this->workingDay;
    }
    public function getVisitCard(){
        $str = $this->getFio() . '<br>';
        $str.= $this->getRole() . '<br>';
        $str.= $this->getMail() . '<br>';
        $str.= $this->getPhone() . '<br>';
        $str.= $this->getWorkingDay() . '<br>';
    }


}