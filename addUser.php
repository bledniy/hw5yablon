<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Add User</h1>
    <form action="users.php" method="post">
        <div>
            <label>Full Name <input type="text" name="full_name"></label>
        </div>
        <div>
            <label>Phone: <input type="text" name="phone"></label>
        </div>
        <div>
            <label>E-mail: <input type="text" name="email"></label>
        </div>
        <div>
            <label>Role:
                <select name="role">
                    <option value="student">Student</option>
                    <option value="teacher">Teacher</option>
                    <option value="admin">Admin</option>
                </select>
            </label>
        </div>
        <div>
            <label>Averange mark: <input type="text" name="mark" placeholder="Only for student"></label>
        </div>
        <div>
            <label>Subject: <input type="text" name="subject" placeholder="Only for teacher"></label>
        </div>
        <div>
            <label>Working day: <input type="text" name="working_day" placeholder="Only for admin"></label>
        </div>
        <button type="submit">Add</button>
    </form>

</body>
</html>
