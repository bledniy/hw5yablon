<?php

require_once 'config/db.php';

try {
    $sql = " CREATE TABLE members (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    phone VARCHAR(255),
    email VARCHAR(255),
    role VARCHAR(255) NOT NULL,
    averange_mark FLOAT NOT NULL,
    subject VARCHAR (255) NOT NULL,
    working_day VARCHAR (255) NOT NULL,
    date_created DATE NOT NULL) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
";
    $pdo->exec($sql);
} catch (Exception $exception) {
    echo 'Error creating table!' . $exception->getCode() . $exception->getMessage();
    die();
}

header('Location:index.php');
